import { instanceArticle as axios} from "../api";

export const SEARCH = "SEARCH";
export const SHOWMORE = "SHOWMORE";

export const search = (query, sortBy = "relevance", page = 0) => dispatch => {
  dispatch({ type: "loading" });
  axios
    .get("articlesearch.json", {
      params: {
        query: query,
        sort: sortBy,
        page: page
      }
    })
    .then(res => {
      dispatch({
        type: SEARCH,
        payload: res.data.response.docs
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({ type: "finish", payload: "Error: " + err });
    });
};

export const showMore = (query, page, sortBy = "best") => dispatch => {
  dispatch({ type: "loading" });
  axios
    .get("articlesearch.json", {
      params: {
        query: query,
        sort: sortBy,
        page: page
      }
    })
    .then(res => {
      dispatch({
        type: SHOWMORE,
        payload: res.data.response.docs
      });
    })
    .catch(err => {
      console.log("error: " + err);
      dispatch({ type: "finish" });
    });
};
