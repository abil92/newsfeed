import { instanceBook as axios} from "../api";

export const SUCCESS = "BOOKSUCCESS";

export const search = (list, keyword) => dispatch => {
  dispatch({ type: "loading" });
  axios
    .get("lists.json", {
      params: {
        list: list,
      }
    })
    .then(res => {
        let finalResult = []
        const filterBy = keyword.toLowerCase();
        res.data.results.map ((book) => {
            const bookName = book.book_details[0].title.toLowerCase();
            if(bookName.indexOf(filterBy) !== -1){
                finalResult.push(book);
            }
        })
      dispatch({
        type: SUCCESS,
        payload: finalResult
      });
      dispatch({type: "finish"});
    })
    .catch(err => {
      console.log(err);
      dispatch({ type: "finish", payload: "Error: " + err });
    });
};