import { applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import Reactotron from "./ReactotronConfig";

import rootReducer from "./reducers";

// const middleware = [thunk];

// const store = createStore(
//   rootReducer,
//   compose(
//     applyMiddleware(...middleware),
//     window.__REDUX_DEVTOOLS_EXTENSION__
//       ? window.__REDUX_DEVTOOLS_EXTENSION__ &&
//         window.__REDUX_DEVTOOLS_EXTENSION__()
//       : f => f
//   )
// );
// const store = createStore(rootReducer, applyMiddleware(thunk));

const middleware = applyMiddleware(thunk);
const store = Reactotron.createStore(rootReducer, compose(middleware));

export default store;
