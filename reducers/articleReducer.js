import { SEARCH, SHOWMORE } from "../actions/article";

const initialState = {
  response: [],
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case "loading":
      return { ...state, loading: true };
    case "finish":
      return { ...state, loading: false };
    case SEARCH:
      return { ...state, response: action.payload, loading: false };
    case SHOWMORE:
      return {
        ...state,
        response: [...state.response, ...action.payload],
        loading: false
      };
    default:
      return state;
  }
}
