import { SUCCESS } from "../actions/book";

const initialState = {
  result: [],
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SUCCESS:
      return { ...state, result: action.payload};
    default:
      return state;
  }
}
