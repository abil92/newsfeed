import { combineReducers } from "redux";
import ArticleReducer from "./articleReducer";
import BookReducer from "./bookReducer";

const rootReducer = combineReducers({
  article: ArticleReducer,
  book: BookReducer,
});

export default rootReducer;
