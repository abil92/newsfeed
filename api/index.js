import axios from "axios";

const instanceArticle = axios.create({
  baseURL: "https://api.nytimes.com/svc/search/v2/",
  params: {
    "api-key": "6KHjYAcmTmOUuwAZr7vP1D7QzUKsDxFc"
  }
});

const instanceBook = axios.create({
  baseURL: "https://api.nytimes.com/svc/books/v3/",
  params: {
    "api-key": "6KHjYAcmTmOUuwAZr7vP1D7QzUKsDxFc"
  }
});

export {instanceArticle};
export {instanceBook};