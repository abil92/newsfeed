import Reactotron from "reactotron-react-native";
import { reactotronRedux } from "reactotron-redux";

const reactotron = Reactotron.configure({
  name: "React Native Demo",
  host: "192.168.88.13"
})
  .useReactNative()
  .use(reactotronRedux())
  .connect();

export default reactotron;
