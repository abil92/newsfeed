import React, { Component } from "react";
import { Image as Img } from "react-native";

export default class Image extends Component {
  render() {
    const { imgUrl } = this.props;
    if (!imgUrl || imgUrl.trim() == "") {
      return <Img source={require("../assets/images/notfound.jpg")} {...this.props} />;
    }
    const imageUri = `https://static01.nyt.com/${imgUrl}`;
    console.log(imageUri)
    return <Img source={{ uri: imageUri}} {...this.props} />;
  }
}
