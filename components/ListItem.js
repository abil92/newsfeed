import React, { Component } from "react";

import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import Image from "./image";

export default class ListItem extends Component {
  constructor(props) {
    super(props);
    this.state = { text: "" };
  }

  render() {
    const { title, desc, imageSrc } = this.props;
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onPress();
        }}
      >
        <View style={styles.listItem}>
          <Image style={{ width: 75, height: 75 }} imgUrl={imageSrc} />
          <View>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.desc}>{desc}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  listItem: {
    flex: 1,
    flexDirection: "row",
    marginHorizontal: 10,
    marginVertical: 10
  },
  title: {
    fontWeight: "bold",
    fontSize: 14
  },
  desc: {}
});
