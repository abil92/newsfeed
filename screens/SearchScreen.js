import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  StyleSheet,
  TextInput,
  FlatList,
  Button,
  Keyboard,
  Picker,
} from "react-native";
import ListItem from "../components/ListItem";
import { search, showMore } from "../actions/article";

class SearchScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { keyword: "", page: 0, sortBy: "relevance" };
  }

  static navigationOptions = {
    title: "Search Article"
  };

  search() {
    this.props.search(this.state.keyword);
    Keyboard.dismiss();
  }

  sort(val) {
    this.setState({
      page: 0,
      sortBy: val
    });
    this.props.search(this.state.keyword, val);
  }

  showMore() {
    const nextPage = this.state.page + 1;
    this.setState({
      page: nextPage
    });
    this.props.showMore(this.state.keyword, nextPage);
  }

  _keyExtractor = (item, index) => item._id;

  renderList() {
    return (
      <View style={{ marginBottom: 180 }}>
        <FlatList
          data={this.props.data}
          keyExtractor={this._keyExtractor}
          renderItem={({ item }) => {
            return (
              <ListItem
                title={item.headline.main}
                desc={item.headline.main}
                imageSrc={
                  item.multimedia.length > 1 ? item.multimedia[2].url : ""
                }
                onPress={() => {
                  this.props.navigation.navigate("Detail", {
                    uri: item.web_url,
                    title: item.headline.main
                  });
                }}
              />
            );
          }}
        />
        {this.props.data.length > 0 && (
          <View style={{ marginHorizontal: 80 }}>
            <Button
              onPress={() => this.showMore()}
              title="Load More"
              color="#841584"
              disabled={this.props.loading}
            />
          </View>
        )}
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          onChangeText={text => this.setState({ keyword: text })}
          value={this.state.keyword}
          placeholder="Search Article"
        />
        <View style={{ marginHorizontal: 80, marginVertical: 10 }}>
          <Button
            onPress={() => this.search()}
            title="Search"
            color="#841584"
            disabled={this.props.loading}
          />
          <Picker
            selectedValue={this.state.sortBy}
            onValueChange={this.sort.bind(this)}
          >
            <Picker.Item label="Relevance" value="relevance" />
            <Picker.Item label="Newest" value="newest" />
            <Picker.Item label="Oldest" value="oldest" />
          </Picker>
        </View>
        {this.renderList()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  textInput: {
    marginHorizontal: 10,
    paddingLeft: 5,
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 10
  },

  item: {
    padding: 10,
    fontSize: 18,
    height: 44
  }
});

const MapStatetoProps = state => ({
  data: state.article.response,
  loading: state.article.loading
});

export default connect(
  MapStatetoProps,
  { search, showMore }
)(SearchScreen);
