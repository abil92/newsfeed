import React, { Component } from "react";
import { View, WebView, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default class Detail extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam("title")
    };
  };

  render() {
    const { navigation } = this.props;
    const webUri = navigation.getParam("uri");

    return (
      <View style={styles.container}>
        <WebView source={{ uri: webUri }} />
      </View>
    );
  }
}
