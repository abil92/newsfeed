import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  StyleSheet,
  Button,
  TextInput,
  FlatList,
  Text,
  Keyboard
} from "react-native";
import { search } from "../actions/book";

class BookScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { keyword: "", page: 0 };
  }

  static navigationOptions = {
    title: "Book"
  };

  search(ebook) {
    if (ebook) {
      this.props.search("e-book-fiction", this.state.keyword);
    } else {
      this.props.search("hardcover-fiction", this.state.keyword);
    }
    Keyboard.dismiss();
  }

  _keyExtractor = (item, index) => item.amazon_product_url;

  renderList() {
    return (
      <View style={{ marginBottom: 90 }}>
        <FlatList
          data={this.props.data}
          keyExtractor={this._keyExtractor}
          renderItem={({ item }) => {
            return (
              <View style={styles.listItem}>
                <Text>Title:</Text>
                <Text style={styles.title}>{item.book_details[0].title}</Text>
                <Text>Desc:</Text>
                <Text>{item.book_details[0].description}</Text>
              </View>
            );
          }}
        />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          onChangeText={text => this.setState({ keyword: text })}
          value={this.state.keyword}
          placeholder="Search Book"
        />
        <View style={{ flexDirection: "row" }}>
          <View style={styles.btn}>
            <Button
              onPress={() => this.search(true)}
              title="E-Book"
              color="#841584"
              disabled={this.props.loading}
            />
          </View>
          <View style={styles.btn}>
            <Button
              onPress={() => this.search(false)}
              title="Hardcover"
              color="#841584"
              disabled={this.props.loading}
            />
          </View>
        </View>
        {this.renderList()}
      </View>
    );
  }
}

const MapStatetoProps = state => ({
  data: state.book.result,
  loading: state.article.loading
});

export default connect(
  MapStatetoProps,
  { search }
)(BookScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  textInput: {
    marginHorizontal: 10,
    paddingLeft: 5,
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 10
  },
  btn: {
    marginHorizontal: 10,
    height: 40,
    justifyContent: "center"
  },
  title: {
    fontWeight: "bold",
    fontSize: 14
  },
  listItem: { marginHorizontal: 15, marginVertical: 15, borderBottomWidth: 1 }
});
